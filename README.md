# debug_evt_flex



> [!NOTE]
> This repo originally lived in BitBucket [here](https://bitbucket.org/cambridgemobiletelematics/debug_evt_flex) but now lives on GitHub [here](https://www.github.com/Censio/debug_evt_flex)
>
> It has been archived in both places and should not be edited

# README #

Contains all Altium files for Debug to EVT connector flex PCB
